﻿
namespace GlobalGamesMVC.Controllers
{
    using System;
    using System.Web.Mvc;
    using Models;
    public class HomeController : Controller
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sobre()
        {
            return View();
        }

        public ActionResult Servicos()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Servicos(Orcamento orcamento)
        {
            if (ModelState.IsValid)
            {
                dc.Orcamento.InsertOnSubmit(orcamento);
            }

            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {

                return View();
            }

        }

        [HttpPost]
        public ActionResult Newsletter(Newsletter newsletter)
        {
            if (ModelState.IsValid)
            {
                dc.Newsletter.InsertOnSubmit(newsletter);
            }

            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}